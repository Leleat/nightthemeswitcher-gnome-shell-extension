# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Night Theme Switcher package.
# Balázs Úr <balazs@urbalazs.hu>, 2021.
# Péter Báthory <bathory86p@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Night Theme Switcher\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-24 11:18+0100\n"
"PO-Revision-Date: 2022-05-04 08:11+0000\n"
"Last-Translator: Péter Báthory <bathory86p@gmail.com>\n"
"Language-Team: Hungarian <https://hosted.weblate.org/projects/night-theme-"
"switcher/extension/hu/>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12.1\n"

#: src/data/ui/BackgroundButton.ui:8
msgid "Change background"
msgstr "Háttér megváltoztatása"

#: src/data/ui/BackgroundButton.ui:24
msgid "Select your background image"
msgstr "Válassza ki a háttérképet"

#: src/data/ui/BackgroundsPage.ui:9
msgid "Backgrounds"
msgstr "Hátterek"

#: src/data/ui/BackgroundsPage.ui:13
msgid ""
"Background switching is handled by the Shell, these settings allow you to "
"change the images it uses."
msgstr ""

#: src/data/ui/BackgroundsPage.ui:16
msgid "Day background"
msgstr "Nappali háttér"

#: src/data/ui/BackgroundsPage.ui:29
msgid "Night background"
msgstr "Éjszakai háttér"

#: src/data/ui/CommandsPage.ui:9
msgid "Commands"
msgstr "Parancsok"

#: src/data/ui/CommandsPage.ui:13
msgid "Run commands"
msgstr "Parancsok futtatása"

#: src/data/ui/CommandsPage.ui:21 src/data/ui/SchedulePage.ui:21
msgid "Sunrise"
msgstr "Napfelkelte"

#: src/data/ui/CommandsPage.ui:28 src/data/ui/SchedulePage.ui:34
msgid "Sunset"
msgstr "Napnyugta"

#: src/data/ui/ContributePage.ui:9
msgid "Contribute"
msgstr "Közreműködés"

#: src/data/ui/ContributePage.ui:42
msgid "View the code and report issues"
msgstr "Nézze át a kódot, és jelentse a hibákat"

#: src/data/ui/ContributePage.ui:55
msgid "Contribute to the translation"
msgstr "Vegyen részt a fordításban"

#: src/data/ui/SchedulePage.ui:9
msgid "Schedule"
msgstr "Ütemezés"

#: src/data/ui/SchedulePage.ui:15
msgid "Manual schedule"
msgstr "Kézi ütemezés"

#: src/data/ui/SchedulePage.ui:16
msgid "Use a manual schedule instead of the one computed from your location."
msgstr ""

#: src/data/ui/SchedulePage.ui:51
msgid "Run the transition when an app is fullscreen"
msgstr ""

#: src/data/ui/SchedulePage.ui:60
msgid "Keyboard shortcut"
msgstr "Gyorsbillentyű"

#: src/data/ui/ShortcutButton.ui:15
msgid "Choose…"
msgstr "Válasszon…"

#: src/data/ui/ShortcutButton.ui:32
msgid "Change keyboard shortcut"
msgstr "Gyorsbillentyű megváltoztatása"

#: src/data/ui/ShortcutButton.ui:43
msgid "Clear"
msgstr "Törlés"

#: src/data/ui/ShortcutButton.ui:78
msgid "Press your keyboard shortcut…"
msgstr "Nyomja meg a gyorsbillentyűt…"

#. Time separator (eg. 08:27)
#: src/data/ui/TimeChooser.ui:33 src/data/ui/TimeChooser.ui:91
msgid ":"
msgstr ":"

#: src/data/ui/TimeChooser.ui:52
msgid "AM"
msgstr ""

#: src/data/ui/TimeChooser.ui:58
msgid "PM"
msgstr ""

#: src/modules/Timer.js:285
#, fuzzy
#| msgid "Button location"
msgid "Unknown Location"
msgstr "Gomb helye"

#: src/modules/Timer.js:286
msgid "A manual schedule will be used to switch the dark mode."
msgstr ""

#: src/modules/Timer.js:290
#, fuzzy
#| msgid "Manual schedule"
msgid "Edit Manual Schedule"
msgstr "Kézi ütemezés"

#: src/preferences/BackgroundButton.js:94
msgid "This image format is not supported."
msgstr ""

#, fuzzy
#~| msgid "Extension Settings"
#~ msgid "Open Location Settings"
#~ msgstr "Kiterjesztés beállításai"

#~ msgid "notify-send \"Hello sunshine!\""
#~ msgstr "notify-send \"Helló, napfény!\""

#~ msgid "notify-send \"Hello moonshine!\""
#~ msgstr "notify-send \"Helló, holdfény!\""

#~ msgid "Support me with a donation"
#~ msgstr "Támogasson adománnyal"

#~ msgid "Themes"
#~ msgstr "Témák"

#~ msgid ""
#~ "GNOME has a built-in dark mode that the extension uses. Manually "
#~ "switching themes is discouraged and is only here for legacy reasons."
#~ msgstr ""
#~ "A GNOME beépített sötét móddal rendelkezik, amelyet a bővítmény használ. "
#~ "A témák kézi váltása nem ajánlott, és csak örökölt okok miatt van itt."

#~ msgid "Switch GTK theme"
#~ msgstr "GTK-téma átváltása"

#~ msgid ""
#~ "Forcing a dark theme on applications not designed to support it can have "
#~ "undesirable side-effects such as unreadable text or invisible icons."
#~ msgstr ""
#~ "A sötét téma erőltetése ezt nem támogató alkalmazásokon nemkívánatos "
#~ "mellékhatásokkal járhat, például olvashatatlan szöveggel vagy láthatatlan "
#~ "ikonokkal."

#~ msgid "Day variant"
#~ msgstr "Nappali változat"

#~ msgid "Night variant"
#~ msgstr "Éjszakai változat"

#~ msgid "Switch Shell theme"
#~ msgstr "Shell-téma átváltása"

#~ msgid "Switch icon theme"
#~ msgstr "Ikontéma átváltása"

#~ msgid "Switch cursor theme"
#~ msgstr "Kurzortéma átváltása"

#~ msgid "Default"
#~ msgstr "Alapértelmezett"

#~ msgid ""
#~ "Only JPEG, PNG, TIFF, SVG and XML files can be set as background image."
#~ msgstr ""
#~ "Csak JPEG, PNG, TIFF, SVG és XML fájlok állíthatók be háttérképként."

#~ msgid "Settings version"
#~ msgstr "Beállítások verziója"

#~ msgid "The current extension settings version"
#~ msgstr "A jelenlegi kiterjesztés beállításainak verziója"

#~ msgid "Enable GTK variants switching"
#~ msgstr "GTK-változatok átváltásának engedélyezése"

#~ msgid "Day GTK theme"
#~ msgstr "Nappali GTK-téma"

#~ msgid "The GTK theme to use during daytime"
#~ msgstr "A nappal használandó GTK-téma"

#~ msgid "Night GTK theme"
#~ msgstr "Éjszakai GTK-téma"

#~ msgid "The GTK theme to use during nighttime"
#~ msgstr "Az éjszaka használandó GTK-téma"

#~ msgid "Enable shell variants switching"
#~ msgstr "Shell-változatok átváltásának engedélyezése"

#~ msgid "Day shell theme"
#~ msgstr "Nappali Shell-téma"

#~ msgid "The shell theme to use during daytime"
#~ msgstr "A nappal használandó Shell-téma"

#~ msgid "Night shell theme"
#~ msgstr "Éjszakai Shell-téma"

#~ msgid "The shell theme to use during nighttime"
#~ msgstr "Az éjszaka használandó Shell-téma"

#~ msgid "Enable icon variants switching"
#~ msgstr "Ikonváltozatok átváltásának engedélyezése"

#~ msgid "Day icon theme"
#~ msgstr "Nappali ikontéma"

#~ msgid "The icon theme to use during daytime"
#~ msgstr "A nappal használandó ikontéma"

#~ msgid "Night icon theme"
#~ msgstr "Éjszakai ikontéma"

#~ msgid "The icon theme to use during nighttime"
#~ msgstr "Az éjszaka használandó ikontéma"

#~ msgid "Enable cursor variants switching"
#~ msgstr "Kurzorváltozatok átváltásának engedélyezése"

#~ msgid "Day cursor theme"
#~ msgstr "Nappali kurzortéma"

#~ msgid "The cursor theme to use during daytime"
#~ msgstr "A nappal használandó kurzortéma"

#~ msgid "Night cursor theme"
#~ msgstr "Éjszakai kurzortéma"

#~ msgid "The cursor theme to use during nighttime"
#~ msgstr "Az éjszaka használandó kurzortéma"

#~ msgid "Enable commands"
#~ msgstr "Parancsok engedélyezése"

#~ msgid "Commands will be spawned on time change"
#~ msgstr "A parancsok az idő megváltozásakor lesznek végrehajtva"

#~ msgid "Sunrise command"
#~ msgstr "Napfelkelte parancs"

#~ msgid "The command to spawn at sunrise"
#~ msgstr "A napfelkeltekor futtatandó parancs"

#~ msgid "Sunset command"
#~ msgstr "Napnyugta parancs"

#~ msgid "The command to spawn at sunset"
#~ msgstr "A napnyugtakor futtatandó parancs"

#~ msgid "Path to the day background"
#~ msgstr "A nappali háttér elérési útja"

#~ msgid "Path to the night background"
#~ msgstr "Az éjszakai háttér elérési útja"

#~ msgid "Time source"
#~ msgstr "Idő forrása"

#~ msgid "The source used to check current time"
#~ msgstr "A jelenlegi idő ellenőrzéséhez használt forrás"

#~ msgid "Follow Night Light \"Disable until tomorrow\""
#~ msgstr "Az éjszakai fény „Letiltás holnapig” funkciójának követése"

#~ msgid "Switch back to day time when Night Light is temporarily disabled"
#~ msgstr ""
#~ "Visszaváltás nappali időre, ha az éjszakai fény átmenetileg le van tiltva"

#~ msgid "Always enable the on-demand timer"
#~ msgstr "Mindig engedélyezze az igény szerinti időzítőt"

#~ msgid "The on-demand timer will always be enabled alongside other timers"
#~ msgstr ""
#~ "Az igény szerinti időzítő mindig engedélyezve lesz a többi időzítő mellett"

#~ msgid "On-demand time"
#~ msgstr "Igény szerinti idő"

#~ msgid "The current time used in on-demand mode"
#~ msgstr "Az igény szerinti módban használt jelenlegi idő"

#~ msgid "Key combination to toggle time"
#~ msgstr "Billentyűkombináció az idő átváltásához"

#~ msgid "The key combination that will toggle time in on-demand mode"
#~ msgstr ""
#~ "Az a billentyűkombináció, amely átváltja az időt igény szerinti módban"

#~ msgid "On-demand button placement"
#~ msgstr "Igény szerinti gomb elhelyezése"

#~ msgid "Where the on-demand button will be placed"
#~ msgstr "Hol lesz az igény szerinti gomb elhelyezve"

#~ msgid "Use manual time source"
#~ msgstr "Kézi időforrás használata"

#~ msgid "Disable automatic time source detection"
#~ msgstr "Időforrás automatikus felismerésének letiltása"

#~ msgid "Sunrise time"
#~ msgstr "Napfelkelte ideje"

#~ msgid "When the day starts"
#~ msgstr "Mikor kezdődik a nap"

#~ msgid "Sunset time"
#~ msgstr "Napnyugta ideje"

#~ msgid "When the day ends"
#~ msgstr "Mikor végződik a nap"

#~ msgid "Transition"
#~ msgstr "Átmenet"

#~ msgid "Use a transition when changing the color scheme"
#~ msgstr "Átmenet használata a színséma módosításakor"

#~ msgid "Manual time source"
#~ msgstr "Kézi időforrás"

#~ msgid ""
#~ "The extension will try to use Night Light or Location Services to "
#~ "automatically set your current sunrise and sunset times if they are "
#~ "enabled. If you prefer, you can manually choose a time source."
#~ msgstr ""
#~ "A kiterjesztés megpróbálja használni az éjszakai fényt vagy a "
#~ "helymeghatározási szolgáltatásokat, hogy automatikusan beállítsa az "
#~ "aktuális napfelkelte és napnyugta idejét, ha ezek engedélyezve vannak. Ha "
#~ "szeretné, akkor kézzel is kiválaszthatja az idő forrását."

#~ msgid "Always show on-demand controls"
#~ msgstr "Mindig jelenítse meg az igény szerinti vezérlőket"

#~ msgid "Allows you to override the current time when using a schedule."
#~ msgstr ""
#~ "Lehetővé teszi a jelenlegi idő felülbírálását, ha ütemezést használ."

#~ msgid "Advanced"
#~ msgstr "Speciális"

#~ msgid "Smooth transition between day and night appearance."
#~ msgstr "Simább átmenet a nappali és éjszakai megjelenés között."

#~ msgid "Night Light"
#~ msgstr "Éjszakai fény"

#~ msgid "These settings only apply when Night Light is the time source."
#~ msgstr ""
#~ "Ezek a beállítások csak akkor érvényesek, amikor az idő forrása az "
#~ "Éjszakai fény."

#~ msgid "Follow <i>Disable until tomorrow</i>"
#~ msgstr "A <i>Letiltás holnapig</i> funkció követése"

#~ msgid ""
#~ "When Night Light is temporarily disabled, the extension will switch to "
#~ "day variants."
#~ msgstr ""
#~ "Ha az éjszakai fény átmenetileg le van tiltva, akkor a kiterjesztés át "
#~ "fog váltani a nappali változatokra."

#~ msgid ""
#~ "These settings only apply when using the manual schedule as the time "
#~ "source."
#~ msgstr "Ezek a beállítások csak a kézi ütemezésű időforrásnál érvényesek."

#~ msgid "On-demand"
#~ msgstr "Igény szerint"

#~ msgid "These settings only apply when using the on-demand time source."
#~ msgstr "Ezek a beállítások csak az igény szerinti időforrásnál érvényesek."

#~ msgid "Turn Night Mode Off"
#~ msgstr "Éjszakai mód kikapcsolása"

#~ msgid "Turn Night Mode On"
#~ msgstr "Éjszakai mód bekapcsolása"

#~ msgid "Night Mode Off"
#~ msgstr "Éjszakai mód ki"

#~ msgid "Night Mode On"
#~ msgstr "Éjszakai mód be"

#~ msgid "Turn Off"
#~ msgstr "Kikapcsolás"

#~ msgid "Turn On"
#~ msgstr "Bekapcsolás"

#~ msgid "Location Services"
#~ msgstr "Helymeghatározó szolgáltatások"

#~ msgid "None"
#~ msgstr "Nincs"

#~ msgid "Top bar"
#~ msgstr "Felső sáv"

#~ msgid "System menu"
#~ msgstr "Rendszermenü"

#~ msgid "Switch GTK variants"
#~ msgstr "GTK-változatok átváltása"

#~ msgid "Switch shell variants"
#~ msgstr "Shell-változatok átváltása"

#~ msgid "Switch icon variants"
#~ msgstr "Ikonváltozatok átváltása"

#~ msgid "Switch cursor variants"
#~ msgstr "Kurzorváltozatok átváltása"

#, fuzzy
#~| msgid "Day GTK theme"
#~ msgid "GTK theme"
#~ msgstr "Nappali GTK-téma"

#, fuzzy
#~| msgid "Day shell theme"
#~ msgid "Shell theme"
#~ msgstr "Nappali Shell-téma"

#, fuzzy
#~| msgid "Day icon theme"
#~ msgid "Icon theme"
#~ msgstr "Nappali ikontéma"

#, fuzzy
#~| msgid "Day cursor theme"
#~ msgid "Cursor theme"
#~ msgstr "Nappali kurzortéma"

#~ msgid "Manual schedule times"
#~ msgstr "Idők kézi ütemezése"

#~ msgid "Enable backgrounds"
#~ msgstr "Hátterek engedélyezése"

#~ msgid "Background will be changed on time change"
#~ msgstr "A hátterek az idő megváltozásakor lesznek megváltoztatva"

#, javascript-format
#~ msgid ""
#~ "Unable to automatically detect the day and night variants for the \"%s\" "
#~ "GTK theme. Please manually choose them in the extension's preferences."
#~ msgstr ""
#~ "Nem lehet automatikusan felismerni a(z) „%s” GTK-téma nappali és éjszakai "
#~ "változatait. Válassza ki kézzel azokat a kiterjesztés beállításaiban."

#, javascript-format
#~ msgid ""
#~ "Unable to automatically detect the day and night variants for the \"%s\" "
#~ "GNOME Shell theme. Please manually choose them in the extension's "
#~ "preferences."
#~ msgstr ""
#~ "Nem lehet automatikusan felismerni a(z) „%s” GNOME Shell témájának "
#~ "nappali és éjszakai változatait. Válassza ki kézzel azokat a kiterjesztés "
#~ "beállításaiban."

#~ msgid "Switch to night theme"
#~ msgstr "Váltás éjszakai témára"

#~ msgid "Switch to day theme"
#~ msgstr "Váltás nappali témára"

#~ msgid "Use manual GTK variants"
#~ msgstr "Kézi GTK-változatok használata"

#~ msgid "Disable automatic GTK theme variants detection"
#~ msgstr "GTK-témaváltozat automatikus felismerésének letiltása"

#~ msgid "Use manual shell variants"
#~ msgstr "Kézi Shell-változatok használata"

#~ msgid "Disable automatic shell theme variants detection"
#~ msgstr "Shell-témaváltozat automatikus felismerésének letiltása"

#~ msgid "Switch backgrounds"
#~ msgstr "Hátterek átváltása"

#~ msgid "Switch cursor theme variants"
#~ msgstr "Kurzortéma-változatok átváltása"

#~ msgid "Manual variants"
#~ msgstr "Kézi változatok"

#~ msgid ""
#~ "You can manually set variants if the extension cannot automatically "
#~ "detect the day and night variants of your GTK theme. Please <a "
#~ "href=\"https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-"
#~ "extension/-/issues\">submit a request</a> to get your theme supported."
#~ msgstr ""
#~ "Kézzel is beállíthatja a változatokat, ha a kiterjesztés nem tudja "
#~ "automatikusan felismerni a GTK-téma nappali és éjszakai változatait. <a "
#~ "href=\"https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-"
#~ "extension/-/issues\">Küldjön egy kérést</a>, hogy támogatva legyen az Ön "
#~ "témája."

#~ msgid "Support us"
#~ msgstr "Támogasson minket"

#~ msgid "View code on GitLab"
#~ msgstr "A forráskód megtekintése GitLabon"

#~ msgid "Translate on Weblate"
#~ msgstr "Fordítás Weblate-en"

#~ msgid "Donate on Liberapay"
#~ msgstr "Támogasson Liberapay-en"

#~ msgid "Appearance"
#~ msgstr "Megjelenés"

#~ msgid ""
#~ "You can manually set variants if the extension cannot automatically "
#~ "detect the day and night variants of your GNOME Shell theme. Please <a "
#~ "href=\"https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-"
#~ "extension/-/issues\">submit a request</a> to get your theme supported."
#~ msgstr ""
#~ "Kézzel is beállíthatja a változatokat, ha a kiterjesztés nem tudja "
#~ "automatikusan felismerni a GNOME Shell témájának nappali és éjszakai "
#~ "változatait. <a href=\"https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-"
#~ "shell-extension/-/issues\">Küldjön egy kérést</a>, hogy támogatva legyen "
#~ "az Ön témája."
